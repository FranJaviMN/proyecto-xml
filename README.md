# Proyecto XML

En este proyecto tendremos un ejercicio realizado en Python que trabajara sobre un fichero .xml .
El fichero de python se basara en un enunciado creado por mi mismo que trabajar sobre el fichero xml que se encunetra a continuacion: [fichero.xml](https://gitlab.com/FranJaviMN/proyecto-xml/-/blob/master/characters.xml)
 
## ENUNCIADO EJERCICIO EN PYTHON
* 1. Listar el nombre de todos los personajes y mostrar los atributos propios de cada uno de ellos.
* 2. Cuenta la cantidad de personajes que hay.
* 3. Se pide por teclado un stat(move, vigour o agility) y la puntuacion de esa stat y se mostrara todos aquellos personajes que tengan esa puntuacion.
* 4. Se pide por teclado el nombre de un ataque y muestra el nombre del personaje que tiene ese ataque.
* 5. Se pide por teclado el nombre de dos personajes y de cada uno de los personajes un ataque, mostrando finalmente el ganador del combate.

