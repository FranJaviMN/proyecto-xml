import random
from Funciones import *
from lxml import etree

doc=etree.parse('characters.xml')

print(mostrar_menu())

eleccion=int(input('Eliga una opcion del menu: '))

while eleccion != 6:


    if eleccion == 1:
        personajes,atributos=personajes_stats(doc)
        for personaje,atributo in zip(personajes,atributos):
            print('El personaje',personaje,'tiene',atributo['Movimiento'],'puntos de movimiento,',atributo['Vigor'],'puntos de vigor y',atributo['Agilidad'],'puntos de agilidad.')
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

    if eleccion == 2:
        print('En el fichero hay una cantidad de',cont_personajes(doc),'personajes.')
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))
    
    if eleccion == 3:
        atributos=['Movimiento','Vigor','Agilidad']
        atributo=str(input('Dime un atributo que consultar: '))
        if atributo in atributos:
            puntuacion=str(input('Dime la puntuacion del atributo: '))
            for personajes in stat_personaje(doc,atributo,puntuacion):
                print('El personaje',personajes,'tiene',puntuacion,'puntos de',atributo)
        else:
            print('No hay un atributo llamado',atributo)
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

    if eleccion == 4:
        arma=str(input('Dime el nombre del ataque que quieres consultar: '))
        print(ataque_personaje(doc,arma))
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

    if eleccion == 5:
        lista_personajes=doc.xpath('//characters/name/text()')

        print('Los personajes a elegir son los siguientes: ')
        for i in lista_personajes:
            print(i)
        personaje1=str(input('Dime el personaje numero 1 que quieres elegir: '))
        personaje2=str(input('Dime el personaje numero 2 que quieres elegir: '))

        if personaje1 in lista_personajes and personaje2 in lista_personajes:

            lista_arma_personaje1=doc.xpath('//characters[name="%s"]/./weapons/name/text()' %(personaje1))
            lista_arma_personaje2=doc.xpath('//characters[name="%s"]/./weapons/name/text()' %(personaje2))

            print('El personaje',personaje1,'tiene los siguientes ataques: ')
            for ataque in lista_arma_personaje1:
                print(ataque)
            arma1=str(input('Dime el ataque que quieres escoger: '))

            print('El personaje',personaje2,'tiene los siguientes ataques: ')
            for ataque in lista_arma_personaje2:
                print(ataque)
            arma2=str(input('Dime el ataque que quieres escoger: '))

            if arma1 in lista_arma_personaje1 and arma2 in lista_arma_personaje2:
                print('El combate ha tenido el siguiente resultado: ')
                print(combate(doc,personaje1,arma1,personaje2,arma2))
        else:
            print('El combate no puede ser posible.')
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))
    
    
    if eleccion > 6:
        print('ERROR, no existe ninguna eleccion numero',eleccion)
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

if eleccion == 6:
    print('Saliendo del programa...')
