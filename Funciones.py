import random

# En esta funcion se listara los nombres de los personajes y sus atributos.

def personajes_stats(doc):
    lista_personajes=doc.xpath('//characters/name/text()')
    lista_movimiento=doc.xpath('//characters/stats/move/text()')
    lista_vigor=doc.xpath('//characters/stats/vigour/text()')
    lista_agilidad=doc.xpath('//characters/stats/agility/text()')
    lista_atributos=[]
    diccionario={}
    for move,vigor,agilidad in zip(lista_movimiento,lista_vigor,lista_agilidad):
        diccionario['Movimiento']=move
        diccionario['Vigor']=vigor
        diccionario['Agilidad']=agilidad
        lista_atributos.append(diccionario)
        diccionario={}
    return lista_personajes,lista_atributos

# En esta funcion vamos a contar la cantidad de personajes que hay en el fichero de xml

def cont_personajes(doc):
    lista_personajes=doc.xpath('//characters/name/text()')
    return len(lista_personajes)

# En esta funcion se pide por teclado una stat y su puntuacion y se mostrara todos aquellos personajes con esa puntuacion.

def stat_personaje(doc,stat,puntuacion):
    lista_movimiento=doc.xpath('//characters/stats/move/text()')
    lista_vigor=doc.xpath('//characters/stats/vigour/text()')
    lista_agilidad=doc.xpath('//characters/stats/agility/text()')
    if stat == 'Movimiento' and puntuacion in lista_movimiento:
        lista_personajes=doc.xpath('//characters/stats[move="%s"]/../name/text()' %(puntuacion))
        return lista_personajes
    if stat == 'Vigor' and puntuacion in lista_vigor:
        lista_personajes=doc.xpath('//characters/stats[vigour="%s"]/../name/text()' %(puntuacion))
        return lista_personajes
    if stat == 'Agilidad' and puntuacion in lista_agilidad:
        lista_personajes=doc.xpath('//characters/stats[agility="%s"]/../name/text()' %(puntuacion))
        return lista_personajes


# En esta funcion se pide el nombre de uno de los ataques y te dice al personaje que pertenece.

def ataque_personaje(doc,arma):
    lista_armas=doc.xpath('//characters/weapons/name/text()')
    if arma in lista_armas:
        personaje=doc.xpath('//characters/weapons[name="%s"]/../name/text()' %(arma))
        for nombre in personaje:
            return nombre
    else:
        return 'No existe el ataque indicado.'

# En la siguiente funcion simularemos un combate entre personajes.

def combate(doc,character1,weapon1,character2,weapon2):
    num_dados_weapon1_character1=doc.xpath('//characters[name="%s"]/weapons[name="%s"]/dices/text()' %(character1,weapon1))
    num_dados_weapon2_character2=doc.xpath('//characters[name="%s"]/weapons[name="%s"]/dices/text()' %(character2,weapon2))
    hit_damage_character1=doc.xpath('//characters[name="%s"]/weapons[name="%s"]/hit/text()' %(character1,weapon1))
    hit_damage_character2=doc.xpath('//characters[name="%s"]/weapons[name="%s"]/hit/text()' %(character2,weapon2))
    damage_weapon1=doc.xpath('//characters[name="%s"]/weapons[name="%s"]/damage/text()' %(character1,weapon1))
    damage_weapon2=doc.xpath('//characters[name="%s"]/weapons[name="%s"]/damage/text()' %(character2,weapon2))
    vigor_character1=doc.xpath('//characters[name="%s"]/stats/vigour/text()' %(character1))
    vigor_character2=doc.xpath('//characters[name="%s"]/stats/vigour/text()' %(character2))
    diccionario_character1={}
    diccionario_character2={}
    for num_dados,hit_damage,damage,vigor in zip(num_dados_weapon1_character1,hit_damage_character1,damage_weapon1,vigor_character1):
        diccionario_character1['Hit para dañar']=hit_damage
        diccionario_character1['Numero de dados']=num_dados
        diccionario_character1['Daño por hit']=damage
        diccionario_character1['Vigor Del personaje']=vigor
    for num_dados,hit_damage,damage,vigor in zip(num_dados_weapon2_character2,hit_damage_character2,damage_weapon2,vigor_character2):
        diccionario_character2['Hit para dañar']=hit_damage
        diccionario_character2['Vigor Del personaje']=vigor
        diccionario_character2['Numero de dados']=num_dados
        diccionario_character2['Daño por hit']=damage

    num_hit_ch1=0
    for i in range(0,int(diccionario_character1['Numero de dados'])):
        if random.randint(1,6) >= int(diccionario_character1['Hit para dañar']):
            num_hit_ch1=num_hit_ch1+1

    num_hit_ch2=0
    for i in range(0,int(diccionario_character2['Numero de dados'])):
        if random.randint(1,6) >= int(diccionario_character2['Hit para dañar']):
            num_hit_ch2=num_hit_ch2+1
    
    damage_ch1=num_hit_ch1*int(diccionario_character1['Daño por hit'])
    damage_ch2=num_hit_ch2*int(diccionario_character2['Daño por hit'])
    if damage_ch1 == 0 and damage_ch2 == 0:
        return 'combate anulado.'
    else:

        salud_restante_ch2=int(diccionario_character1['Vigor Del personaje'])-damage_ch1
        salud_restante_ch1=int(diccionario_character2['Vigor Del personaje'])-damage_ch2
        if salud_restante_ch1 > 0 and salud_restante_ch2 > 0:
            while salud_restante_ch1 > 0 and salud_restante_ch2 > 0:
                salud_restante_ch2=salud_restante_ch2-damage_ch1
                salud_restante_ch1=salud_restante_ch1-damage_ch2

    if salud_restante_ch1 <= 0:
        return 'Jugador 1 ha ganado'
    else:
        return 'Jugador 2 ha ganado'


def mostrar_menu():
    menu='''
    --------------------MENU--------------------
    1. Listar el nombre de todos los personajes y mostrar los atributos propios de cada uno de ellos.
    2. Cuenta la cantidad de personajes que hay.
    3. Se pide por teclado un stat(move, vigour o agility) y la puntuacion de esa stat y se mostrara todos aquellos personajes que tengan esa puntuacion.
    4. Se pide por teclado el nombre de un ataque y muestra el nombre del personaje que tiene ese ataque.
    5. Se pide por teclado el nombre de dos personajes y de cada uno de los personajes un ataque, mostrando finalmente el ganador del combate.
    6. Salir del programa
    '''
    return menu
